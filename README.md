Authors: PZ, PS, DR

***IMPORTANT: The game can only be compiled using specific program and can only be working on a MSP430 Easy Web device.***

***Here's the link under which you can see the game demonstration: https://photos.app.goo.gl/bzxzw2fMzNZbucxJ8*** 

For a Computer Architecture project we decided to create a „Dino” game (similar to na https://chromedino.com/).

There are four options which can be selected by clicking buttons:

• Play (GRA)
• HiSc (HIGHSCORE)
• Desc (OPIS)
• Auth (AUTORZY)

Letters on consecutive positions are selected by clicking button 1 and 2. Button 3 lets the user select the new letter. Button 4 starts the game.

Dino jumps over cactuses after clicking button 1. A diode is lit at the same time. When a cactus is jumped over, a point is added to the score.

Once Dino hits a cactus, the game ends and the score is displayed. If the previous best score is lower than the current one, it gets overritten.
